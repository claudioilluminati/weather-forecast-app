package com.claudioilluminati.weather.android.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.view.KeyEvent;

import com.claudioilluminati.weather.android.R;

/**
 * Created by claudioilluminati on 23/11/14.
 */
public class WaitDownloadForecastDialog extends DialogFragment {
    static Context mContext;
    static AlertDialog.Builder alertDialogBuilder;

    public static final WaitDownloadForecastDialog newInstance(
            final Context context) {
        WaitDownloadForecastDialog fragment = new WaitDownloadForecastDialog();
        mContext = context;
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setTitle(mContext.getString(R.string.wait_please_msg));
        alertDialogBuilder.setMessage(mContext.getString(R.string.download_updated_weather_forecast));

        // Disable the back button
        new OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        };

        return alertDialogBuilder.create();
    }

}