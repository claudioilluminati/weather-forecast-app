package com.claudioilluminati.weather.android.wwo;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by claudioilluminati on 22/11/14.
 */
public class LocalWeather extends WwoApi {

    public static final String FREE_API_ENDPOINT = "http://api.worldweatheronline.com/free/v2/weather.ashx";

    public Data callAPI(String query) {
        return getLocalWeatherData(getInputStream(FREE_API_ENDPOINT + query));
    }

    Data getLocalWeatherData(InputStream is) {

        Data weather = new Data();
        ArrayList<Weather> weatherForecastNextDays = new ArrayList<Weather>();
        CurrentCondition cc = new CurrentCondition();

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            System.out.println("Root element :"
                    + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("current_condition");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    cc.setTemp_C(eElement.getElementsByTagName("temp_C")
                            .item(0).getTextContent());
                    cc.setTemp_F(eElement.getElementsByTagName("temp_F")
                            .item(0).getTextContent());
                    cc.setWeatherIconUrl(eElement.getElementsByTagName("weatherIconUrl")
                            .item(0).getTextContent());
                    cc.setWeatherDesc(eElement.getElementsByTagName("weatherDesc")
                            .item(0).getTextContent());
                    cc.setWindspeedMiles(eElement.getElementsByTagName("windspeedMiles")
                            .item(0).getTextContent());
                    cc.setWindspeedKmph(eElement.getElementsByTagName("windspeedKmph")
                            .item(0).getTextContent());
                    cc.setWinddir16Point(eElement.getElementsByTagName("winddir16Point")
                            .item(0).getTextContent());
                    cc.setPrecipMM(eElement.getElementsByTagName("precipMM")
                            .item(0).getTextContent());
                    cc.setWeatherDesc(eElement.getElementsByTagName("weatherDesc")
                            .item(0).getTextContent());
                    cc.setHumidity(eElement.getElementsByTagName("humidity")
                            .item(0).getTextContent());
                    cc.setPressure(eElement.getElementsByTagName("pressure")
                            .item(0).getTextContent());

                }
            }

            nList = doc.getElementsByTagName("weather");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Weather weatherElement = new Weather();

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    weatherElement.setDate(eElement.getElementsByTagName("date")
                            .item(0).getTextContent());
                    weatherElement.setTempMinC(eElement.getElementsByTagName("mintempC")
                            .item(0).getTextContent());
                    weatherElement.setTempMaxC(eElement.getElementsByTagName("maxtempC")
                            .item(0).getTextContent());
                    weatherElement.setTempMinF(eElement.getElementsByTagName("mintempF")
                            .item(0).getTextContent());
                    weatherElement.setTempMaxF(eElement.getElementsByTagName("maxtempF")
                            .item(0).getTextContent());

                    NodeList hourlyList = eElement.getElementsByTagName("hourly");
                    Hourly hourly = new Hourly();
                    for (int cont = 0; cont < hourlyList.getLength(); cont++) {
                        Node nNodeHourly = hourlyList.item(cont);

                        System.out.println("\nCurrent Element :" + nNodeHourly.getNodeName());

                        if (nNodeHourly.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElementHourly = (Element) nNodeHourly;
                            if (eElementHourly.getElementsByTagName("time")
                                    .item(0).getTextContent().equalsIgnoreCase("1300")) {
                                hourly.setWeatherIconUrl(eElementHourly.getElementsByTagName("weatherIconUrl")
                                        .item(0).getTextContent());
                                hourly.setWeatherDesc(eElementHourly.getElementsByTagName("weatherDesc")
                                        .item(0).getTextContent());
                                break;
                            }
                            continue;
                        }
                    }
                    weatherElement.setHourly(hourly);
                }

                weatherForecastNextDays.add(weatherElement);
            }

            weather.current_condition = cc;
            if (weatherForecastNextDays.size() > 0) {
                weather.weather = weatherForecastNextDays;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weather;
    }


    public class Params extends WwoApi.RootParams {
        String q;                    //required
        String num_of_days = "1";        //required
        String show_comments = "no";
        String key;                    //required

        public Params(String key) {
            show_comments = "no";
            this.key = key;
        }

        public Params setQ(String q) {
            this.q = q;
            return this;
        }

        public Params setNumOfDays(String num_of_days) {
            this.num_of_days = num_of_days;
            return this;
        }

    }

    public class Data {
        CurrentCondition current_condition;

        ArrayList<Weather> weather;

        public CurrentCondition getCurrent_condition() {
            return current_condition;
        }

        public ArrayList<Weather> getWeather() {
            return weather;
        }

    }

    public class CurrentCondition {
        String observation_time;
        String temp_C;
        String temp_F;
        String weatherCode;
        String weatherIconUrl;
        String weatherDesc;
        String windspeedMiles;
        String windspeedKmph;
        String winddirDegree;
        String winddir16Point;
        String precipMM;
        String humidity;
        String visibility;
        String pressure;
        String cloudcover;

        public String getTemp_C() {
            return temp_C;
        }

        public void setTemp_C(String temp_C) {
            this.temp_C = temp_C;
        }

        public String getTemp_F() {
            return temp_F;
        }

        public void setTemp_F(String temp_F) {
            this.temp_F = temp_F;
        }

        public String getWeatherIconUrl() {
            return weatherIconUrl;
        }

        public void setWeatherIconUrl(String weatherIconUrl) {
            this.weatherIconUrl = weatherIconUrl;
        }

        public String getWeatherDesc() {
            return weatherDesc;
        }

        public void setWeatherDesc(String weatherDesc) {
            this.weatherDesc = weatherDesc;
        }

        public String getWindspeedMiles() {
            return windspeedMiles;
        }

        public void setWindspeedMiles(String windspeedMiles) {
            this.windspeedMiles = windspeedMiles;
        }

        public String getWindspeedKmph() {
            return windspeedKmph;
        }

        public void setWindspeedKmph(String windspeedKmph) {
            this.windspeedKmph = windspeedKmph;
        }

        public String getWinddir16Point() {
            return winddir16Point;
        }

        public void setWinddir16Point(String winddir16Point) {
            this.winddir16Point = winddir16Point;
        }

        public String getPrecipMM() {
            return precipMM;
        }

        public void setPrecipMM(String precipMM) {
            this.precipMM = precipMM;
        }

        public String getHumidity() {
            return humidity;
        }

        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }

        public String getPressure() {
            return pressure;
        }

        public void setPressure(String pressure) {
            this.pressure = pressure;
        }

        @Override
        public String toString() {
            return "CurrentCondition{" +
                    "observation_time='" + observation_time + '\'' +
                    ", temp_C='" + temp_C + '\'' +
                    ", temp_F='" + temp_F + '\'' +
                    ", weatherCode='" + weatherCode + '\'' +
                    ", weatherIconUrl='" + weatherIconUrl + '\'' +
                    ", weatherDesc='" + weatherDesc + '\'' +
                    ", windspeedMiles='" + windspeedMiles + '\'' +
                    ", windspeedKmph='" + windspeedKmph + '\'' +
                    ", winddirDegree='" + winddirDegree + '\'' +
                    ", winddir16Point='" + winddir16Point + '\'' +
                    ", precipMM='" + precipMM + '\'' +
                    ", humidity='" + humidity + '\'' +
                    ", visibility='" + visibility + '\'' +
                    ", pressure='" + pressure + '\'' +
                    ", cloudcover='" + cloudcover + '\'' +
                    '}';
        }
    }

    public class Weather {
        String date;
        String tempMaxC;
        String tempMaxF;
        String tempMinC;
        String tempMinF;
        String windspeedMiles;
        String windspeedKmph;
        String winddirection;
        String weatherCode;
        String weatherIconUrl;
        String weatherDesc;
        String precipMM;
        Hourly hourly;

        public Hourly getHourly() {
            return hourly;
        }

        public void setHourly(Hourly hourly) {
            this.hourly = hourly;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTempMaxC() {
            return tempMaxC;
        }

        public String getTempMaxF() {
            return tempMaxF;
        }

        public void setTempMaxC(String tempMaxC) {
            this.tempMaxC = tempMaxC;
        }

        public void setTempMaxF(String tempMaxF) {
            this.tempMaxF = tempMaxF;
        }

        public void setTempMinC(String tempMinC) {
            this.tempMinC = tempMinC;
        }

        public void setTempMinF(String tempMinF) {
            this.tempMinF = tempMinF;
        }


        @Override
        public String toString() {
            return "Weather{" +
                    "date='" + date + '\'' +
                    ", tempMaxC='" + tempMaxC + '\'' +
                    ", tempMaxF='" + tempMaxF + '\'' +
                    ", tempMinC='" + tempMinC + '\'' +
                    ", tempMinF='" + tempMinF + '\'' +
                    ", windspeedMiles='" + windspeedMiles + '\'' +
                    ", windspeedKmph='" + windspeedKmph + '\'' +
                    ", winddirection='" + winddirection + '\'' +
                    ", weatherCode='" + weatherCode + '\'' +
                    ", weatherIconUrl='" + weatherIconUrl + '\'' +
                    ", weatherDesc='" + weatherDesc + '\'' +
                    ", precipMM='" + precipMM + '\'' +
                    '}';
        }
    }

    public class Hourly {

        String time;
        String weatherIconUrl;
        String weatherDesc;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getWeatherIconUrl() {
            return weatherIconUrl;
        }

        public void setWeatherIconUrl(String weatherIconUrl) {
            this.weatherIconUrl = weatherIconUrl;
        }

        public String getWeatherDesc() {
            return weatherDesc;
        }

        public void setWeatherDesc(String weatherDesc) {
            this.weatherDesc = weatherDesc;
        }

    }
}
