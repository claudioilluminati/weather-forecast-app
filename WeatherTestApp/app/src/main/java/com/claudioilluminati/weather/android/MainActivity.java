package com.claudioilluminati.weather.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.claudioilluminati.weather.android.dialog.WaitDownloadForecastDialog;
import com.claudioilluminati.weather.android.fragment.ForecastFragment;
import com.claudioilluminati.weather.android.fragment.NavigationDrawerFragment;
import com.claudioilluminati.weather.android.fragment.TodayWeatherFragment;
import com.claudioilluminati.weather.android.util.CheckInternetConnection;
import com.claudioilluminati.weather.android.wwo.LocalWeather;
import com.claudioilluminati.weather.android.wwo.LocationSearch;
import com.claudioilluminati.weather.android.wwo.MyLocation;

/**
 * Created by claudioilluminati on 22/11/14.
 */
public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final int SETTINGS_REQUEST = 1982;

    static LocalWeather.Data weather;
    static LocationSearch.Data loc;

    private static final String DIALOGTAG = "WaitDownloadForecastDialog";

    TodayWeatherFragment todayWeatherFragment = new TodayWeatherFragment();
    ForecastFragment forecastFragment = new ForecastFragment();
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private int temperatureType;
    private int lengthType;

    private int drawableIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        final SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        temperatureType = Integer.valueOf(sharedPrefs.getString("temperature","1"));
        lengthType = Integer.valueOf(sharedPrefs.getString("lenght","1"));

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        if (CheckInternetConnection
                .isNetworkAvailable(getApplicationContext())) {
            // show dialog
            final WaitDownloadForecastDialog donwloadDialog = WaitDownloadForecastDialog
                    .newInstance(getApplicationContext());
            donwloadDialog.show(getSupportFragmentManager(), DIALOGTAG);
            donwloadDialog.setCancelable(false);

            downloadWeatherData();
        } else {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.no_internet_connection),
                    Toast.LENGTH_LONG).show();
        }

    }

    MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {
        @Override
        public void gotLocation(Location location) {

            String q = Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude());

            //get weather
            LocalWeather lw = new LocalWeather();
            String query = (lw.new Params(lw.FREE_API_KEY)).setQ(q).setNumOfDays("5").getQueryString(LocalWeather.Params.class);
            weather = lw.callAPI(query);

            //get location
            LocationSearch ls = new LocationSearch();
            query = (ls.new LocationParams(ls.FREE_API_KEY)).setQuery(q).getQueryString(LocationSearch.LocationParams.class);
            loc = ls.callAPI(query);

            if( weather != null && loc != null) {
                todayWeatherFragment.updateView(weather, loc);
            }

            final WaitDownloadForecastDialog downloadDialog = (WaitDownloadForecastDialog) getSupportFragmentManager()
                    .findFragmentByTag(DIALOGTAG);
            if (downloadDialog != null) {
                if (downloadDialog.getDialog() != null) {
                    downloadDialog.getDialog().dismiss();
                }
            }

        }
    };


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        switch (position) {
            case 0:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, todayWeatherFragment)
                        .commitAllowingStateLoss();
                break;
            case 1:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, forecastFragment)
                        .commitAllowingStateLoss();
                break;
        }

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(mTitle);
        actionBar.setIcon(drawableIcon);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mNavigationDrawerFragment != null && !mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final Intent intent = new Intent();
            intent.setClass(this, SettingsActivity.class);
            startActivityForResult(intent, SETTINGS_REQUEST);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SETTINGS_REQUEST) {
            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(this);
            int temperatureTypeFromPreferences = Integer.valueOf(sharedPrefs.getString("temperature","1"));
            int lenghtTypeFromPreferences = Integer.valueOf(sharedPrefs.getString("lenght","1"));
            if (temperatureTypeFromPreferences!=temperatureType || lenghtTypeFromPreferences != lengthType) {
                todayWeatherFragment.updateView(weather,loc);
                temperatureType = temperatureTypeFromPreferences;
                lengthType = lenghtTypeFromPreferences;
            }
        }
    }

    private void downloadWeatherData() {
        MyLocation myLocation = new MyLocation();
        myLocation.getLocation(getApplicationContext(), locationResult);
    }

    public static LocalWeather.Data getWeather() {
        return weather;
    }

    public static LocationSearch.Data getLocation() {
        return loc;
    }

}
