package com.claudioilluminati.weather.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.net.URL;

/**
 * Created by claudioilluminati on 23/11/14.
 */
public class BitmapUtil {

    private static final String TAGLOG = BitmapUtil.class.getName();

    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((new URL(url)).openConnection().getInputStream());
        } catch (Exception e) {
            Log.e(TAGLOG,"error " + e);
            return null;
        }

        return bitmap;
    }
}
