package com.claudioilluminati.weather.android.wwo;

import android.os.StrictMode;
import android.util.Log;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by claudioilluminati on 22/11/14.
 */
public class WwoApi {
    public static final String TAGLOG = WwoApi.class.getName();
    public String FREE_API_KEY = "0e78db7c824cb3837fe7c35889611";

    static InputStream getInputStream(String url) {
        InputStream is = null;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            is = (new URL(url)).openConnection().getInputStream();
        } catch (Exception e) {
            Log.e(TAGLOG, "" + e);
        }

        return is;
    }


    public class RootParams {
        public String getQueryString(Class cls) {
            String query = null;

            Field[] fields = cls.getDeclaredFields();

            try {
                for (Field field : fields) {
                    Object f = field.get(this);
                    if (f != null) {
                        if (query == null)
                            query = "?" + URLEncoder.encode(field.getName(), "UTF-8") + "="
                                    + URLEncoder.encode((String) f, "UTF-8");
                        else
                            query += "&" + URLEncoder.encode(field.getName(), "UTF-8") + "="
                                    + URLEncoder.encode((String) f, "UTF-8");
                    }
                }
            } catch (Exception e) {
                Log.e(TAGLOG, e.getMessage());
            }

            return query;
        }
    }
}
