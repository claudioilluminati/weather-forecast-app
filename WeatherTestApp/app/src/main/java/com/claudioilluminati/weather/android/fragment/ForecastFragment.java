package com.claudioilluminati.weather.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.claudioilluminati.weather.android.MainActivity;
import com.claudioilluminati.weather.android.R;
import com.claudioilluminati.weather.android.adapter.ForecastAdapter;
import com.claudioilluminati.weather.android.dialog.ShareForecastDialog;
import com.claudioilluminati.weather.android.util.RecyclerItemClickListener;
import com.claudioilluminati.weather.android.wwo.LocalWeather;

/**
 * Created by claudioilluminati on 23/11/14.
 */
public class ForecastFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ForecastAdapter mAdapter;

    public static final String TAGLOG = ForecastFragment.class.getName();

    private static final String DIALOGTAG = "ShareForecastDialog";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAGLOG, "creation of ForecastFragment");

        View rootView = inflater.inflate(R.layout.fragment_forecast, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity().getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // show dialog
                        final ShareForecastDialog shareForecastDialog = ShareForecastDialog
                                .newInstance(getActivity());
                        shareForecastDialog.show(getFragmentManager(), DIALOGTAG);
                    }
                })
        );
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        LocalWeather.Data weather = MainActivity.getWeather();
        if (weather != null) {
            mAdapter = new ForecastAdapter(weather.getWeather(), R.layout.row_forecast, getActivity().getApplicationContext());
            mRecyclerView.setAdapter(mAdapter);
        }

        return rootView;
    }

}
