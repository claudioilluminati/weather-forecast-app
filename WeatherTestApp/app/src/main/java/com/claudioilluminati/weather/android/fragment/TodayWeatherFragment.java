package com.claudioilluminati.weather.android.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.claudioilluminati.weather.android.MainActivity;
import com.claudioilluminati.weather.android.R;
import com.claudioilluminati.weather.android.wwo.LocalWeather;
import com.claudioilluminati.weather.android.wwo.LocationSearch;
import com.squareup.picasso.Picasso;

/**
 * Created by claudioilluminati on 22/11/14.
 */
public class TodayWeatherFragment extends Fragment {

    ImageView iconToday;
    TextView city;
    TextView temperature;
    ImageView humidityIcon;
    ImageView precipitationIcon;
    ImageView pressureIcon;
    TextView humidity;
    TextView precipitation;
    TextView pressure;
    ImageView windSpeedIcon;
    ImageView directionIcon;
    TextView windSpeed;
    TextView direction;
    SharedPreferences prefs;

    private int temperaturePreference = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        View rootView = inflater.inflate(R.layout.fragment_today_weather, container, false);
        iconToday = (ImageView) rootView.findViewById(R.id.icon_today);
        iconToday.setVisibility(View.INVISIBLE);
        city = (TextView) rootView.findViewById(R.id.city);
        temperature = (TextView) rootView.findViewById(R.id.temperature);
        humidityIcon = (ImageView) rootView.findViewById(R.id.humidity_icon);
        humidityIcon.setVisibility(View.INVISIBLE);
        precipitationIcon = (ImageView) rootView.findViewById(R.id.precipitation_icon);
        precipitationIcon.setVisibility(View.INVISIBLE);
        pressureIcon = (ImageView) rootView.findViewById(R.id.pressure_icon);
        pressureIcon.setVisibility(View.INVISIBLE);
        humidity = (TextView) rootView.findViewById(R.id.humidity);
        precipitation = (TextView) rootView.findViewById(R.id.precipitation);
        pressure = (TextView) rootView.findViewById(R.id.pressure);
        windSpeedIcon = (ImageView) rootView.findViewById(R.id.wind_speed_icon);
        windSpeedIcon.setVisibility(View.INVISIBLE);
        directionIcon = (ImageView) rootView.findViewById(R.id.direction_icon);
        directionIcon.setVisibility(View.INVISIBLE);
        windSpeed = (TextView) rootView.findViewById(R.id.wind_speed);
        direction = (TextView) rootView.findViewById(R.id.direction);

        LocalWeather.Data weather = MainActivity.getWeather();
        LocationSearch.Data location = MainActivity.getLocation();
        if (weather != null && location != null) {
            updateView(weather, location);
        }


        return rootView;
    }

    public void updateView(LocalWeather.Data weather, LocationSearch.Data loc) {

        if (getActivity() != null) {
            iconToday.setVisibility(View.VISIBLE);
            humidityIcon.setVisibility(View.VISIBLE);
            precipitationIcon.setVisibility(View.VISIBLE);
            pressureIcon.setVisibility(View.VISIBLE);
            windSpeedIcon.setVisibility(View.VISIBLE);
            directionIcon.setVisibility(View.VISIBLE);

            if (weather != null) {
                if (weather.getCurrent_condition().getWeatherIconUrl() != null) {
                    Picasso.with(getActivity().getApplicationContext()).load(weather.getCurrent_condition().getWeatherIconUrl()).into(iconToday);
                }


                temperaturePreference = Integer.valueOf(prefs.getString("temperature", "1"));
                switch (temperaturePreference) {
                    case 1:
                        temperature.setText(weather.getCurrent_condition().getTemp_C() + " °C  |  " + weather.getCurrent_condition().getWeatherDesc());
                        break;
                    case 2:
                        temperature.setText(weather.getCurrent_condition().getTemp_F() + " °F  |  " + weather.getCurrent_condition().getWeatherDesc());
                        break;
                }

                humidity.setText(weather.getCurrent_condition().getHumidity() + " %");
                precipitation.setText(weather.getCurrent_condition().getPrecipMM() + " mm");
                pressure.setText(weather.getCurrent_condition().getPressure() + " hPa");
                int lenghtPreference = Integer.valueOf(prefs.getString("lenght","1"));
                switch (lenghtPreference) {
                    case 1:
                        windSpeed.setText(weather.getCurrent_condition().getWindspeedKmph() + " Km/h");
                        break;
                    case 2:
                        windSpeed.setText(weather.getCurrent_condition().getWindspeedMiles() + " Miles/h");
                        break;
                }

                direction.setText(weather.getCurrent_condition().getWinddir16Point());
            }

            if (loc != null) {
                city.setText(loc.getAreaName() + "," + loc.getCountry());
            }
        }
    }

}
