package com.claudioilluminati.weather.android.util;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by claudioilluminati on 23/11/14.
 */
public class DateUtil {

    public static String fromDateToDayOfWeek (String input_date, String dateFormat) {
        if (input_date == null || dateFormat == null) {
            return null;
        }

        try {
            SimpleDateFormat format1=new SimpleDateFormat(dateFormat);
            Date dt1 = format1.parse(input_date);
            DateFormat format2=new SimpleDateFormat("EEEE");
            return format2.format(dt1);

        } catch (ParseException e) {
            Log.e("DateUtil", "error = " + e);
            return null;
        }

    }
}
