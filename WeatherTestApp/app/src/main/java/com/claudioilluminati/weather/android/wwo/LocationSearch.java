package com.claudioilluminati.weather.android.wwo;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by claudioilluminati on 22/11/14.
 */
public class LocationSearch extends WwoApi {
    public String FREE_API_ENDPOINT = "http://api.worldweatheronline.com/free/v2/search.ashx";

    public Data callAPI(String query) {
        return getLocationSearchData(getInputStream(FREE_API_ENDPOINT + query));
    }

    Data getLocationSearchData(InputStream is) {

        Data location = new Data();

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            System.out.println("Root element :"
                    + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("result");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    location.setAreaName(eElement.getElementsByTagName("areaName")
                            .item(0).getTextContent());
                    location.setCountry(eElement.getElementsByTagName("country")
                            .item(0).getTextContent());
                    location.setRegion(eElement.getElementsByTagName("region")
                            .item(0).getTextContent());
                    location.setLatitude(eElement.getElementsByTagName("latitude")
                            .item(0).getTextContent());
                    location.setLongitude(eElement.getElementsByTagName("longitude")
                            .item(0).getTextContent());
                    location.setPopulation(eElement.getElementsByTagName("population")
                            .item(0).getTextContent());
                    location.setWeatherUrl(eElement.getElementsByTagName("weatherUrl")
                            .item(0).getTextContent());

                    return location;
                }
            }

        } catch (Exception e) {
            Log.e(TAGLOG, "[LocationSearch} Error " + e);
            return null;
        }

        return null;
    }

    public class LocationParams extends RootParams {
        String query;                    //required
        String num_of_results = "1";
        String key;                    //required

        public LocationParams(String key) {
            num_of_results = "1";
            this.key = key;
        }

        public LocationParams setQuery(String query) {
            this.query = query;
            return this;
        }

    }

    public class Data {
        String areaName;
        String country;
        String region;
        String latitude;
        String longitude;
        String population;
        String weatherUrl;

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public void setPopulation(String population) {
            this.population = population;
        }

        public void setWeatherUrl(String weatherUrl) {
            this.weatherUrl = weatherUrl;
        }
    }
}