package com.claudioilluminati.weather.android.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.claudioilluminati.weather.android.R;
import com.claudioilluminati.weather.android.util.DateUtil;
import com.claudioilluminati.weather.android.wwo.LocalWeather;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by claudioilluminati on 22/11/14.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private ArrayList<LocalWeather.Weather> weather;
    private int rowLayout;
    private Context mContext;

    public ForecastAdapter(ArrayList<LocalWeather.Weather> weather, int rowLayout, Context context) {
        this.weather = weather;
        this.rowLayout = rowLayout;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String date = weather.get(i).getDate();
        String temperature = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        int temperaturePreference = Integer.valueOf(prefs.getString("temperature","1"));
        switch (temperaturePreference) {
            case 1:
                temperature = weather.get(i).getTempMaxC() + " °C";
                break;
            case 2:
                temperature = weather.get(i).getTempMaxF() + " °F";
                break;
        }

        String urlIcon = weather.get(i).getHourly().getWeatherIconUrl();
        String description = weather.get(i).getHourly().getWeatherDesc();
        viewHolder.day.setText(DateUtil.fromDateToDayOfWeek(date,"yyyy-MM-dd"));
        viewHolder.temperature.setText(temperature);
        Picasso.with(mContext).load(urlIcon).into(viewHolder.icon);
        viewHolder.weatherDescription.setText(description);
    }

    @Override
    public int getItemCount() {
        return weather == null ? 0 : weather.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView day;
        public ImageView icon;
        public TextView temperature;
        public TextView weatherDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.weatherIcon);
            day = (TextView) itemView.findViewById(R.id.day);
            temperature = (TextView) itemView.findViewById(R.id.temperature);
            weatherDescription = (TextView) itemView.findViewById(R.id.description);
        }

    }
}
