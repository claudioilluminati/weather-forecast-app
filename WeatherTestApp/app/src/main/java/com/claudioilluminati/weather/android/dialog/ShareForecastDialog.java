package com.claudioilluminati.weather.android.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.claudioilluminati.weather.android.R;

import java.util.ArrayList;

/**
 * Created by claudioilluminati on 24/11/14.
 */
public class ShareForecastDialog  extends DialogFragment {
    static Context mContext;
    static AlertDialog.Builder alertDialogBuilder;

    ArrayList<String> elementsList =  new ArrayList<String>();

    public static final ShareForecastDialog newInstance(
            final Context context) {
        ShareForecastDialog fragment = new ShareForecastDialog();
        mContext = context;
        return fragment;
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setDismissMessage(null);
        super.onDestroyView();
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(mContext.getString(R.string.actions));
        ListView list = new ListView(getActivity());

        String[] array = mContext.getResources().getStringArray(R.array.share_dialog_element);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, array);

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(mContext,
                        "Not yet implemented",
                        Toast.LENGTH_LONG).show();
            }
        });

        builder.setView(list);

        return builder.create();
    }

}